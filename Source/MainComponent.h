#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "../../../common/FMODHeaders.h"


class MainContentComponent : public Component,
                             public ButtonListener,
                             public SliderListener,
                             public Timer
{
private:
    
    Studio::System* system;
    Studio::EventInstance* eventInstance;
    Studio::ParameterInstance* paramInstance;
    
    ToggleButton startStopButton;
    
    Slider timeSlider;
    
public:
	MainContentComponent()
	{
    startStopButton.setButtonText("Start/Stop");
        
        addAndMakeVisible(&timeSlider);
        
    addAndMakeVisible(&startStopButton);
        
        timeSlider.setRange (0.0, 1.0, 0.01);
        
        startStopButton.addListener(this);
        timeSlider.addListener(this);
        
        initFMODStudio();
        startTimer  (40);
        
        setSize (300, 200);
	}
	
	~MainContentComponent()
	{
        stopTimer();
        shutdownFMODStudio();
	}
	
  void initFMODStudio()
	{
        File resourcesPath = getResourcesPath();
        
        File bankPath = resourcesPath.getChildFile("Master Bank.bank");
        
        File stringsPath = resourcesPath.getChildFile("Master Bank.strings.bank");
        
        ERRCHECK (Studio::System::create(&system));
        
        ERRCHECK (system->initialize(64, FMOD_STUDIO_INIT_NORMAL, FMOD_STUDIO_INIT_NORMAL, 0));
        
        Studio::Bank* bank;
        
        ERRCHECK (system->loadBankFile(bankPath.getFullPathName().toUTF8(), FMOD_STUDIO_LOAD_BANK_NORMAL, &bank));
        
        Studio::Bank* stringsBank;
        
        ERRCHECK (system->loadBankFile (stringsPath.getFullPathName().toUTF8(), FMOD_STUDIO_LOAD_BANK_NORMAL, &stringsBank));
        
        const char* eventPath = "event:/Country";
        
        Studio::EventDescription* eventDescription;
        
        ERRCHECK (system->getEvent(eventPath, &eventDescription));
        
        ERRCHECK (eventDescription->createInstance (&eventInstance));
        
        ERRCHECK (eventInstance->getParameter("time",&paramInstance));
	}
    
	void shutdownFMODStudio()
	{
        ERRCHECK (eventInstance->release());
    }
	
	void resized() override
	{
        startStopButton.setBounds (10, 30, getWidth()-20, 20);
        timeSlider.setBounds (10, 60, getWidth()-20, 20);
	}
    
    void buttonClicked (Button* button) override
    {
        if (&startStopButton == button)
        {
            const bool isPlaying = startStopButton.getToggleState();
            
            if (isPlaying)
            {
                ERRCHECK (eventInstance->start());
            }
            
            else
            {
                ERRCHECK (eventInstance->stop (FMOD_STUDIO_STOP_ALLOWFADEOUT));
            }
        }
    }
    
    void sliderValueChanged (Slider* slider) override
    {
        if (&timeSlider == slider)
        {
            const float time = (float) timeSlider.getValue();
            
            ERRCHECK (paramInstance->setValue(time));
        }
    }
    
    void timerCallback() override
    {
        ERRCHECK (system->update());
    }

};

#endif  // MAINCOMPONENT_H_INCLUDED
