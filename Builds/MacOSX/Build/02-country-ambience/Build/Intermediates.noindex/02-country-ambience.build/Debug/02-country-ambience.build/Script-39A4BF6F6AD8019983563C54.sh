#!/bin/sh
if ! [ -e "$TARGET_BUILD_DIR/$PRODUCT_NAME.app/Contents/Frameworks" ]
then
mkdir "$TARGET_BUILD_DIR/$PRODUCT_NAME.app/Contents/Frameworks"
fi
cp -f ../../../../../libs/fmod/fmodstudioapi/api/lowlevel/lib/libfmod$FMODLIBSUFFIX "$TARGET_BUILD_DIR/$PRODUCT_NAME.app/Contents/Frameworks/libfmod$FMODLIBSUFFIX"
cp -f ../../../../../libs/fmod/fmodstudioapi/api/studio/lib/libfmodstudio$FMODLIBSUFFIX "$TARGET_BUILD_DIR/$PRODUCT_NAME.app/Contents/Frameworks/libfmodstudio$FMODLIBSUFFIX"

install_name_tool -change @rpath/libfmod$FMODLIBSUFFIX @loader_path/../Frameworks/libfmod$FMODLIBSUFFIX "$TARGET_BUILD_DIR/$PRODUCT_NAME.app/Contents/MacOS/$PRODUCT_NAME"
install_name_tool -change @rpath/libfmod$FMODLIBSUFFIX @loader_path/../Frameworks/libfmod$FMODLIBSUFFIX "$TARGET_BUILD_DIR/$PRODUCT_NAME.app/Contents/Frameworks/libfmodstudio$FMODLIBSUFFIX"
install_name_tool -change @rpath/libfmodstudio$FMODLIBSUFFIX @loader_path/../Frameworks/libfmodstudio$FMODLIBSUFFIX "$TARGET_BUILD_DIR/$PRODUCT_NAME.app/Contents/MacOS/$PRODUCT_NAME"

if [ -e "$TARGET_BUILD_DIR/$PRODUCT_NAME.app/Contents/Resources/Media" ]
then
rm -R "$TARGET_BUILD_DIR/$PRODUCT_NAME.app/Contents/Resources/Media"
fi

if [ -e ../../Media ]
then
cp -R ../../Media "$TARGET_BUILD_DIR/$PRODUCT_NAME.app/Contents/Resources/Media"
fi

if [ -e ../../MediaSource/$PRODUCT_NAME/Build/Desktop ]
then
cp -R ../../MediaSource/$PRODUCT_NAME/Build/Desktop "$TARGET_BUILD_DIR/$PRODUCT_NAME.app/Contents/Resources/Media"
fi

if [ -e ../../MediaSource/$PRODUCT_NAME/Build/GUIDs.txt ]
then
cp ../../MediaSource/$PRODUCT_NAME/Build/GUIDs.txt "$TARGET_BUILD_DIR/$PRODUCT_NAME.app/Contents/Resources/GUIDs.txt"
fi

if [ -e ../../MediaSource/$PRODUCT_NAME/Build/GUIDs-mixer.txt ]
then
cp ../../MediaSource/$PRODUCT_NAME/Build/GUIDs-mixer.txt "$TARGET_BUILD_DIR/$PRODUCT_NAME.app/Contents/Resources/GUIDs-mixer.txt"
fi

if [ -e "$TARGET_BUILD_DIR/$PRODUCT_NAME.app/Contents/Resources/Media/Game" ]
then
rm -R "$TARGET_BUILD_DIR/$PRODUCT_NAME.app/Contents/Resources/Media/Game"
fi

if [ -e ../../Game/mac ]
then
cp -R ../../Game/mac "$TARGET_BUILD_DIR/$PRODUCT_NAME.app/Contents/Resources/Media/Game"
fi

